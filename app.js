

function calculateRandomValue(minimumValue, maximumValue) {

    result = Math.floor(Math.random() * (maximumValue - minimumValue) + minimumValue);
    return result;
};

// function computePlayerandMonsterStyles() {
//     if (battleEvent.instigator === player.name) {
//         return 'log--player'
//     }
//     else {
//         return 'log--monster'
//     }
// }


const app = Vue.createApp({
    data() {
        return {
            gameActive: true,
            winner: null,
            player: {
                name: 'Player',
                health: 100
            },
            enemy:
            {
                name: 'Monster',
                health: 100
            },
            roundsPlayed: 0,
            battleLog: []
        }
    },
    methods: {
        logAction(instigator, target, actionType, value, valueType) {
            console.log(`${instigator} ${actionType} ${target} for ${value} ${valueType}`);
            const round = this.roundsPlayed;
            let turnLog = {
                instigator,
                target,
                actionType,
                value,
                valueType,
                round
            };
            this.battleLog.push(turnLog);
            console.log(this.battleLog);
        },
        playerAttackMonster() {
            const playerAttackValue = calculateRandomValue(5, 12);

            this.enemy.health -= playerAttackValue;
            this.logAction(this.player.name, this.enemy.name, 'attacked', playerAttackValue, 'damage');
            this.monsterAttackPlayer();

        },
        specialPlayerAttack() {
            const specialAttackValue = calculateRandomValue(10, 25);
            this.logAction(this.player.name, this.enemy.name, 'special attacked', specialAttackValue, 'damage');
            this.enemy.health -= specialAttackValue;
            this.monsterAttackPlayer();
        },
        monsterAttackPlayer() {
            const monsterAttackValue = calculateRandomValue(8, 15);
            this.logAction(this.enemy.name, this.player.name, 'attacked', monsterAttackValue, 'damage');
            this.player.health -= monsterAttackValue;
            this.roundsPlayed++;

        },
        healPlayer() {
            const healValue = calculateRandomValue(8, 20);

            if (this.player.health + healValue > 100) {
                this.player.health = 100;
            } else {
                this.player.health += healValue;
            }

            this.logAction(this.player.name, this.player.name, 'healed', healValue, 'health');

            this.monsterAttackPlayer();
        },
        endGame() {
            this.gameActive = false;
        },
        startGame() {
            this.player.health = 100;
            this.enemy.health = 100;
            this.gameActive = true;
            this.winner = null;
            this.roundsPlayed = 0;
            this.battleLog = [];
        },
        surrender() {
            this.endGame();
            this.player.health = 0;
            this.winner = this.enemy.name;
        }



    },
    computed: {
        monsterHealthBarStyles() {
            if (this.enemy.health < 0) {
                return { width: '0%' };
            } else {
                return {
                    width: `${this.enemy.health}%`
                }
            }

        },
        playerHealthBarStyles() {
            if (this.player.health < 0) {
                return { width: '0%' }
            } else {
                return {
                    width: `${this.player.health}%`
                }
            }
        },
        specialAvailable() {
            return this.roundsPlayed % 3 !== 0;
        },




    },

    watch: {
        player: {
            deep: true,
            handler() {
                if (this.player.health > 0 && this.enemy.health <= 0) {
                    console.log('Player has won');
                    this.endGame();
                    this.winner = this.player.name;
                }
                else if (this.player.health <= 0 && this.enemy.health > 0) {
                    console.log('Monster has won');
                    this.endGame();
                    this.winner = this.enemy.name;
                }
                else if (this.player.health <= 0 && this.enemy.health <= 0) {
                    console.log(`It's a draw`);
                    this.endGame();
                    this.winner = "Draw";

                }
                else {
                    console.log('The battle rages on');
                }

            }

        }
    }


})

app.mount('#game');

